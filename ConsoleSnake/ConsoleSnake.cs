﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ConsoleSnake
{

    class ConsoleSnake
    {

        private static ConsoleSnake instance;
        private static readonly Object lockObject = new Object();
        public static readonly int TICKS = 60;
        private Boolean running = false;
        private RenderManager renderManager;
        private InputManager inputManager;
        private EntityManager entityManager;
        private IGamemode gamemode;
        private UI ui;

        public ConsoleSnake()
        {

            if(instance != null)
            {

                return;

            }

            instance = this;
            this.initialize();

        }

        private void initialize()
        {

            this.setRenderManager(new RenderManager());
            this.setInputManager(new InputManager());
            this.getInputManager().register(new UIController());
            this.setEntityManager(new EntityManager());

        }

        public void start()
        {

            if(this.isRunning())
            {

                return;

            }

            this.setRunning(true);
            Console.CursorVisible = false;
            this.setUI(new UIMainMenu());
            this.run();

        }

        public void stop()
        {

            if(!this.isRunning())
            {

                return;

            }

            this.setRunning(false);
            Environment.Exit(0);

        }

        private void run()
        {

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            double previous = stopwatch.ElapsedMilliseconds;
            double lag = 0.0;
            double timePrTick = 1000 / TICKS;

            while(this.isRunning())
            {

                double current = stopwatch.ElapsedMilliseconds;
                double elapsed = current - previous;
                previous = current;
                lag += elapsed;

                while(lag >= timePrTick)
                {

                    this.update();
                    lag -= timePrTick;

                }

                this.render();

            }

        }

        private void update()
        {

            this.getInputManager().poll();

            if (this.getUI() != null)
            {

                this.getUI().update();

                for (int i = 0; i < this.getUI().getControlObjects().Count; i++)
                {

                    this.getUI().getControlObjects()[i].update();

                }

            }

        }

        private void render()
        {

            this.getRenderManager().render();

        }

        static void Main(string[] args)
        {

            ConsoleSnake.getInstance().start();

        }

        /*
         * 
         * Getters & Setters
         * 
         * */

        public static ConsoleSnake getInstance()
        {

            if(instance == null)
            {

                lock(lockObject)
                {

                    if(instance == null)
                    {

                        new ConsoleSnake();

                    }

                }

            }

            return instance;

        }

        public Boolean isRunning()
        {

            return this.running;

        }

        private void setRunning(Boolean running)
        {

            this.running = running;

        }

        public RenderManager getRenderManager()
        {

            return this.renderManager;

        }

        private void setRenderManager(RenderManager renderManager)
        {

            this.renderManager = renderManager;

        }

        public InputManager getInputManager()
        {

            return this.inputManager;

        }

        private void setInputManager(InputManager inputManager)
        {

            this.inputManager = inputManager;

        }

        public EntityManager getEntityManager()
        {

            return this.entityManager;

        }

        public void setEntityManager(EntityManager entityManager)
        {

            this.entityManager = entityManager;

        }

        public IGamemode getGamemode()
        {

            return this.gamemode;

        }

        public void setGamemode(IGamemode gamemode)
        {

            this.gamemode = gamemode;

            if(gamemode != null)
            {

                gamemode.start();

            }

        }

        public UI getUI()
        {

            return this.ui;

        }

        public void setUI(UI ui)
        {

            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.Clear();
            this.ui = ui;

        }
    
    }

}