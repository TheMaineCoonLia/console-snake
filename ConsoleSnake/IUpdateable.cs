﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleSnake
{

    interface IUpdateable
    {

        void update();

        int getUpdateCounter();

        void setUpdateCounter(int updateCount);

        void incrementUpdateCounter(int amount);

        void decrementUpdateCounter(int amount);

        void incrementUpdateCounter();

        void decrementUpdateCounter();

        void resetUpdateCounter();

    }

}