﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleSnake
{

    class OneColumnLayout : ILayout
    {

        private UI ui;
        private int updateCounter;

        public OneColumnLayout(UI ui)
        {

            this.setUI(ui);

        }

        public void update()
        {

            for(int i = 0; i < this.getUI().getControlObjects().Count; i++)
            {

                ControlObject controlObject = this.getUI().getControlObjects()[i];
                controlObject.setX((Console.WindowWidth / 2) - (controlObject.getWidth() / 2));

            }

        }

        public UI getUI()
        {

            return this.ui;

        }

        private void setUI(UI ui)
        {

            this.ui = ui;

        }

        public int getUpdateCounter()
        {

            return this.updateCounter;

        }

        public void setUpdateCounter(int updateCounter)
        {

            this.updateCounter = updateCounter;

        }

        public void incrementUpdateCounter(int amount)
        {

            this.setUpdateCounter(this.getUpdateCounter() + amount);

        }

        public void decrementUpdateCounter(int amount)
        {

            this.setUpdateCounter(this.getUpdateCounter() - amount);

        }

        public void incrementUpdateCounter()
        {

            this.incrementUpdateCounter(1);

        }

        public void decrementUpdateCounter()
        {

            this.decrementUpdateCounter(1);

        }

        public void resetUpdateCounter()
        {

            this.setUpdateCounter(0);

        }

    }

}