﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleSnake
{

    class UIMainMenu : UI
    {

        public UIMainMenu() : base(null)
        {

        }

        public override void init()
        {

            this.getControlObjects().Clear();
            this.getControlObjects().Add(new Button(0, "Play", 5, 5));
            this.getControlObjects().Add(new Button(1, "Multiplayer", 5, 7));
            this.getControlObjects().Add(new Button(2, "Options", 5, 9));
            this.getControlObjects().Add(new Button(3, "Exit", 5, 11));
            this.setLayout(new OneColumnLayout(this));
            base.init();

        }

        public override void render()
        {

        }

        public override void onAction(int code)
        {

            switch(code)
            {

                case 0:

                    ConsoleSnake.getInstance().setUI(new UISinglePlayer(this));

                    break;

                case 3:

                    ConsoleSnake.getInstance().stop();

                    break;

            }

        }

    }

}