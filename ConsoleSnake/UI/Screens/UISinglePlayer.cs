﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleSnake
{

    class UISinglePlayer : UI
    {

        public UISinglePlayer(UI ui)
            : base(ui)
        {

        }

        public override void init()
        {

            this.getControlObjects().Clear();
            this.getControlObjects().Add(new TextField("Name", 5, 5));
            this.getControlObjects().Add(new ColorSelector(1, "Color", 5, 7));
            this.getControlObjects().Add(new Button(0, "Play", 5, 9));
            this.getControlObjects().Add(new Button(1, "Back", 5, 11));
            this.setLayout(new OneColumnLayout(this));
            base.init();

        }

        public override void render()
        {

        }

        public override void onAction(int code)
        {

            switch(code)
            {

                case 0:

                    ConsoleSnake.getInstance().setGamemode(
                        
                        new SinglePlayerGamemode(
                            
                            new EntityPlayer(
                            
                                ((TextField) this.getControlObjects()[0]).getInput(),
                                ((ColorSelector) this.getControlObjects()[1]).getColor()

                            )
                            
                        )
                    
                    );
                    ConsoleSnake.getInstance().setUI(null);

                    break;

                case 1:

                    ConsoleSnake.getInstance().setUI(this.getParent());

                    break;

            }

        }

    }

}