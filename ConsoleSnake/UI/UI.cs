﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleSnake
{

    abstract class UI : IRenderable, IUpdateable
    {

        private UI parent;
        private int updateCounter;
        private readonly List<ControlObject> controlObjects = new List<ControlObject>();
        private ILayout layout;

        public UI() : this(null)
        {

        }

        public UI(UI parent)
        {

            this.setParent(parent);
            this.resetUpdateCounter();
            this.getControlObjects().Clear();
            this.init();

        }

        public abstract void render();

        public virtual void update()
        {

            this.incrementUpdateCounter();

            if(this.getLayout() != null)
            {

                this.getLayout().update();

            }

        }

        public virtual Boolean doesPauseGame()
        {

            return true;

        }

        public virtual void init()
        {

            for(int i = 0; i < this.getControlObjects().Count; i++)
            {

                ControlObject controlObject = this.getControlObjects()[i];

                if(controlObject is Button)
                {

                    ((Button) controlObject).setSelected(true);

                    return;

                }

            }

        }

        public virtual void onKeyHit(ConsoleKeyInfo key)
        {

            for(int i = 0; i < this.getControlObjects().Count; i++)
            {

                ControlObject controlObject = this.getControlObjects()[i];

                if(controlObject.isSelected())
                {

                    controlObject.onKeyHit(key);

                }

            }

        }

        public virtual void onAction(int code)
        {

        }

        public void close()
        {

        }

        public UI getParent()
        {

            return this.parent;

        }

        private void setParent(UI parent)
        {

            this.parent = parent;

        }

        public int getUpdateCounter()
        {

            return this.updateCounter;

        }

        public void setUpdateCounter(int updateCounter)
        {

            this.updateCounter = updateCounter;

        }

        public void incrementUpdateCounter(int amount)
        {

            this.setUpdateCounter(this.getUpdateCounter() + amount);

        }

        public void decrementUpdateCounter(int amount)
        {

            this.setUpdateCounter(this.getUpdateCounter() - amount);

        }

        public void incrementUpdateCounter()
        {

            this.incrementUpdateCounter(1);

        }

        public void decrementUpdateCounter()
        {

            this.decrementUpdateCounter(1);

        }

        public void resetUpdateCounter()
        {

            this.setUpdateCounter(0);

        }

        public List<ControlObject> getControlObjects()
        {

            return this.controlObjects;

        }

        public ILayout getLayout()
        {

            return this.layout;

        }

        public void setLayout(ILayout layout)
        {

            this.layout = layout;

            if(this.getLayout() != null)
            {

                this.getLayout().update();

            }

        }

    }

}