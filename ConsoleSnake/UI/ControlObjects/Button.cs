﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleSnake
{

    class Button : ControlObject
    {

        private int code;
        private String text;
        private Boolean selected;
        private int x;
        private int y;
        private Boolean background;
        private int updateCounter;
        private int alignment;

        public Button(int code, String text, int x, int y) : this(code, text, x, y, 1)
        {

        }

        public Button(int code, String text, int x, int y, int alignment)
        {
            
            this.setCode(code);
            this.setText(text);
            this.setX(x);
            this.setY(y);
            this.setAlignment(alignment);

        }

        public virtual void render()
        {

            Console.SetCursorPosition(this.getX(), this.getY());
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.BackgroundColor = this.isSelected() ? this.shouldDrawBackground() ? ConsoleColor.Green : ConsoleColor.Black : ConsoleColor.Black;
            Console.Write(this.getText());

        }

        public virtual void update()
        {

            if(this.getUpdateCounter() >= ConsoleSnake.TICKS / 2)
            {

                this.resetUpdateCounter();
                this.toggleBackground();

            }

            this.incrementUpdateCounter();

        }

        public virtual void onKeyHit(ConsoleKeyInfo key)
        {

        }

        public int getCode()
        {

            return this.code;

        }

        private void setCode(int code)
        {

            this.code = code;

        }

        public String getText()
        {

            return this.text;

        }

        private void setText(String text)
        {

            this.text = text;

        }

        public Boolean isSelected()
        {

            return this.selected;

        }

        public void setSelected(Boolean selected)
        {

            this.selected = selected;

            if(this.isSelected())
            {

                this.resetUpdateCounter();
                this.setBackground(true);

            }

        }

        public int getX()
        {

            return this.x;

        }

        public void setX(int x)
        {

            this.x = x;

        }

        public int getY()
        {

            return this.y;

        }

        public void setY(int y)
        {

            this.y = y;

        }

        public Boolean shouldDrawBackground()
        {

            return this.background;

        }

        private void setBackground(Boolean background)
        {

            this.background = background;

        }

        private void toggleBackground()
        {

            this.setBackground(this.shouldDrawBackground() ? false : true);

        }

        public int getUpdateCounter()
        {

            return this.updateCounter;

        }

        public void setUpdateCounter(int updateCounter)
        {

            this.updateCounter = updateCounter;

        }

        public void incrementUpdateCounter(int amount)
        {

            this.setUpdateCounter(this.getUpdateCounter() + amount);

        }

        public void decrementUpdateCounter(int amount)
        {

            this.setUpdateCounter(this.getUpdateCounter() - amount);

        }

        public void incrementUpdateCounter()
        {

            this.incrementUpdateCounter(1);

        }

        public void decrementUpdateCounter()
        {

            this.decrementUpdateCounter(1);

        }

        public void resetUpdateCounter()
        {

            this.setUpdateCounter(0);

        }

        public int getAlignment()
        {

            return this.alignment;

        }

        public void setAlignment(int alignment)
        {

            this.alignment = alignment;

        }

        public int getWidth()
        {

            return this.getText().Length;

        }

    }

}