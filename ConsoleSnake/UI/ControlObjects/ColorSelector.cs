﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleSnake
{

    class ColorSelector : ControlObject
    {

        private int code;
        private String text;
        private Boolean selected;
        private int x;
        private int y;
        private Boolean background;
        private int updateCounter;
        private int alignment;
        private ConsoleColor[] colors;
        private ConsoleColor color;

        public ColorSelector(int code, String text, int x, int y)
            : this(code, text, x, y, 1)
        {

        }

        public ColorSelector(int code, String text, int x, int y, int alignment)
            : this(code, text, x, y, new ConsoleColor[]
            {

                ConsoleColor.Blue,
                ConsoleColor.DarkBlue,
                ConsoleColor.Cyan,
                ConsoleColor.DarkCyan,
                ConsoleColor.Gray,
                ConsoleColor.DarkGray,
                ConsoleColor.Green,
                ConsoleColor.DarkGreen,
                ConsoleColor.Magenta,
                ConsoleColor.DarkMagenta,
                ConsoleColor.Red,
                ConsoleColor.DarkRed,
                ConsoleColor.Yellow,
                ConsoleColor.DarkYellow,
                ConsoleColor.White,
                ConsoleColor.Black

            }, 1)
        {

        }

        public ColorSelector(int code, String text, int x, int y, ConsoleColor[] colors)
            : this(code, text, x, y, colors, 1)
        {

        }

        public ColorSelector(int code, String text, int x, int y, ConsoleColor[] colors, int alignment)
        {
            
            this.setCode(code);
            this.setText(text);
            this.setX(x);
            this.setY(y);
            this.setAlignment(alignment);
            this.setColors(colors);
            this.setColor(colors[0]);

        }

        public virtual void render()
        {

            Console.SetCursorPosition(this.getX(), this.getY());
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.BackgroundColor = this.isSelected() ? this.shouldDrawBackground() ? ConsoleColor.Green : ConsoleColor.Black : ConsoleColor.Black;
            Console.Write(this.getText() + ": ");
            Console.SetCursorPosition(this.getX() + this.getWidth() - 1, this.getY());
            Console.ForegroundColor = this.getColor();
            Console.BackgroundColor = this.getColor();
            Console.Write(" ");

        }

        public virtual void update()
        {

            if(this.getUpdateCounter() >= ConsoleSnake.TICKS / 2)
            {

                this.resetUpdateCounter();
                this.toggleBackground();

            }

            this.incrementUpdateCounter();

        }

        public void previous()
        {

            for(int i = 0; i < this.getColors().Length; i++)
            {

                ConsoleColor color = this.getColors()[i];

                if(color == this.getColor())
                {

                    if(i == 0)
                    {

                        this.setColor(this.getColors()[this.getColors().Length - 1]);

                        return;

                    }

                    this.setColor(this.getColors()[i - 1]);

                    return;

                }

            }

        }

        public void next()
        {

            for (int i = 0; i < this.getColors().Length; i++)
            {

                ConsoleColor color = this.getColors()[i];

                if (color == this.getColor())
                {

                    if (i == (this.getColors().Length - 1))
                    {

                        this.setColor(this.getColors()[0]);

                        return;

                    }

                    this.setColor(this.getColors()[i + 1]);

                    return;

                }

            }

        }

        public virtual void onKeyHit(ConsoleKeyInfo key)
        {

        }

        public int getCode()
        {

            return this.code;

        }

        private void setCode(int code)
        {

            this.code = code;

        }

        public String getText()
        {

            return this.text;

        }

        private void setText(String text)
        {

            this.text = text;

        }

        public Boolean isSelected()
        {

            return this.selected;

        }

        public void setSelected(Boolean selected)
        {

            this.selected = selected;

            if(this.isSelected())
            {

                this.resetUpdateCounter();
                this.setBackground(true);

            }

        }

        public int getX()
        {

            return this.x;

        }

        public void setX(int x)
        {

            this.x = x;

        }

        public int getY()
        {

            return this.y;

        }

        public void setY(int y)
        {

            this.y = y;

        }

        public Boolean shouldDrawBackground()
        {

            return this.background;

        }

        private void setBackground(Boolean background)
        {

            this.background = background;

        }

        private void toggleBackground()
        {

            this.setBackground(this.shouldDrawBackground() ? false : true);

        }

        public int getUpdateCounter()
        {

            return this.updateCounter;

        }

        public void setUpdateCounter(int updateCounter)
        {

            this.updateCounter = updateCounter;

        }

        public void incrementUpdateCounter(int amount)
        {

            this.setUpdateCounter(this.getUpdateCounter() + amount);

        }

        public void decrementUpdateCounter(int amount)
        {

            this.setUpdateCounter(this.getUpdateCounter() - amount);

        }

        public void incrementUpdateCounter()
        {

            this.incrementUpdateCounter(1);

        }

        public void decrementUpdateCounter()
        {

            this.decrementUpdateCounter(1);

        }

        public void resetUpdateCounter()
        {

            this.setUpdateCounter(0);

        }

        public int getAlignment()
        {

            return this.alignment;

        }

        public void setAlignment(int alignment)
        {

            this.alignment = alignment;

        }

        public ConsoleColor[] getColors()
        {

            return this.colors;

        }

        private void setColors(ConsoleColor[] colors)
        {

            this.colors = colors;

        }

        public ConsoleColor getColor()
        {

            return this.color;

        }

        private void setColor(ConsoleColor color)
        {

            this.color = color;

        }

        public int getWidth()
        {

            return this.getText().Length + 3;

        }

    }

}