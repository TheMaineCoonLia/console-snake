﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace ConsoleSnake
{

    class TextField : ControlObject
    {

        private String text;
        private String input;
        private Boolean selected;
        private int x;
        private int y;
        private Boolean background;
        private int updateCounter;
        private int alignment;

        public TextField(String text, int x, int y)
            : this(text, x, y, 1)
        {

        }

        public TextField(String text, int x, int y, int alignment)
        {

            this.setText(text);
            this.setInput("");
            this.setX(x);
            this.setY(y);
            this.setAlignment(alignment);

        }

        public virtual void render()
        {

            Console.SetCursorPosition(this.getX(), this.getY());
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write(this.getText() + ": " + this.getInput());
            Console.ForegroundColor = this.shouldDrawBackground() ? this.isSelected() ? ConsoleColor.Green : ConsoleColor.Black : ConsoleColor.Black;
            Console.SetCursorPosition(this.getX() + this.getText().Length + 2 + this.getInput().Length, this.getY());
            Console.Write(this.shouldDrawBackground() ? "_" : " ");

        }

        public virtual void update()
        {

            if (this.getUpdateCounter() >= ConsoleSnake.TICKS / 2)
            {

                this.resetUpdateCounter();
                this.toggleBackground();

            }

            this.incrementUpdateCounter();

        }

        public virtual void onKeyHit(ConsoleKeyInfo key)
        {

            if(!char.IsControl(key.KeyChar))
            {

                this.setInput(this.getInput() + key.KeyChar);

            }
            else if(key.Key == ConsoleKey.Backspace)
            {

                if(this.getInput().Equals(""))
                {

                    return;

                }

                this.setInput(this.getInput().Substring(0, this.getInput().Length - 1));
                Console.SetCursorPosition(this.getX(), this.getY());
                Console.Write(" ");
                Console.SetCursorPosition(this.getX() + this.getWidth() + 1, this.getY());
                Console.Write(" ");

            }

        }

        public String getText()
        {

            return this.text;

        }

        private void setText(String text)
        {

            this.text = text;

        }

        public String getInput()
        {

            return this.input;

        }

        private void setInput(String input)
        {

            this.input = input;

        }

        public Boolean isSelected()
        {

            return this.selected;

        }

        public void setSelected(Boolean selected)
        {

            this.selected = selected;

            if (this.isSelected())
            {

                this.resetUpdateCounter();
                this.setBackground(true);

            }

        }

        public int getX()
        {

            return this.x;

        }

        public void setX(int x)
        {

            this.x = x;

        }

        public int getY()
        {

            return this.y;

        }

        public void setY(int y)
        {

            this.y = y;

        }

        public Boolean shouldDrawBackground()
        {

            return this.background;

        }

        private void setBackground(Boolean background)
        {

            this.background = background;

        }

        private void toggleBackground()
        {

            this.setBackground(this.shouldDrawBackground() ? false : true);

        }

        public int getUpdateCounter()
        {

            return this.updateCounter;

        }

        public void setUpdateCounter(int updateCounter)
        {

            this.updateCounter = updateCounter;

        }

        public void incrementUpdateCounter(int amount)
        {

            this.setUpdateCounter(this.getUpdateCounter() + amount);

        }

        public void decrementUpdateCounter(int amount)
        {

            this.setUpdateCounter(this.getUpdateCounter() - amount);

        }

        public void incrementUpdateCounter()
        {

            this.incrementUpdateCounter(1);

        }

        public void decrementUpdateCounter()
        {

            this.decrementUpdateCounter(1);

        }

        public void resetUpdateCounter()
        {

            this.setUpdateCounter(0);

        }

        public int getAlignment()
        {

            return this.alignment;

        }

        public void setAlignment(int alignment)
        {

            this.alignment = alignment;

        }

        public int getWidth()
        {

            return this.getText().Length + 2 + this.getInput().Length;

        }

    }

}