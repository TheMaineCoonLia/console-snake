﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleSnake
{

    interface ControlObject : IRenderable, IUpdateable
    {

        int getX();

        void setX(int x);

        int getY();

        void setY(int y);

        Boolean isSelected();

        void setSelected(Boolean selected);

        void onKeyHit(ConsoleKeyInfo key);

        int getAlignment();

        void setAlignment(int alignment);

        int getWidth();

    }

}