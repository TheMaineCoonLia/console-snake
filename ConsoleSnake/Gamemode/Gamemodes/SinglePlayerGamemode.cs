﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleSnake
{

    class SinglePlayerGamemode : IGamemode
    {

        private int updateCounter;
        private EntityPlayer player;

        public SinglePlayerGamemode(EntityPlayer player)
        {

            this.setPlayer(player);
            this.resetUpdateCounter();

        }

        public void start()
        {

            int d = Console.WindowHeight - 1;

            for (int i = 0; i < d; i++)
            {

                if (i == 0 || (i == d - 1))
                {

                    for (int j = 0; j < Console.WindowWidth; j++)
                    {

                        ConsoleSnake.getInstance().getEntityManager().spawn(new EntityWall(j, i));

                    }

                }
                else
                {

                    ConsoleSnake.getInstance().getEntityManager().spawn(new EntityWall(0, i));
                    ConsoleSnake.getInstance().getEntityManager().spawn(new EntityWall(Console.WindowWidth - 1, i));

                }

            }

            ConsoleSnake.getInstance().getEntityManager().spawn(this.getPlayer());

        }

        public void stop()
        {

        }

        public void update()
        {

        }

        public void render()
        {

            ConsoleSnake.getInstance().getEntityManager().render();

        }

        public EntityPlayer getPlayer()
        {

            return this.player;

        }

        private void setPlayer(EntityPlayer player)
        {

            this.player = player;

        }

        public int getUpdateCounter()
        {

            return this.updateCounter;

        }

        public void setUpdateCounter(int updateCounter)
        {

            this.updateCounter = updateCounter;

        }

        public void incrementUpdateCounter(int amount)
        {

            this.setUpdateCounter(this.getUpdateCounter() + amount);

        }

        public void decrementUpdateCounter(int amount)
        {

            this.setUpdateCounter(this.getUpdateCounter() - amount);

        }

        public void incrementUpdateCounter()
        {

            this.incrementUpdateCounter(1);

        }

        public void decrementUpdateCounter()
        {

            this.decrementUpdateCounter(1);

        }

        public void resetUpdateCounter()
        {

            this.setUpdateCounter(0);

        }

    }

}