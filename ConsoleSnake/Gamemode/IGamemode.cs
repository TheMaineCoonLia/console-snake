﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleSnake
{

    interface IGamemode : IUpdateable, IRenderable
    {

        void start();

        void stop();

    }

}