﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleSnake
{

    class Entity : IRenderable, IUpdateable
    {

        private String name;
        private int posX;
        private int posY;
        private int oldPosX;
        private int oldPosY;
        private int updateCounter;

        public Entity(String name)
        {

            this.setName(name);
            this.resetUpdateCounter();

        }

        public virtual void update()
        {

        }

        public virtual void render()
        {

        }

        public String getName()
        {

            return this.name;

        }

        private void setName(String name)
        {

            this.name = name;

        }

        public int getPosX()
        {

            return this.posX;

        }

        public void setPosX(int posX)
        {

            this.posX = posX;

        }

        public int getPosY()
        {

            return this.posY;

        }

        public void setPosY(int posY)
        {

            this.posY = posY;

        }

        public int getOldPosX()
        {

            return this.oldPosX;

        }

        public void setOldPosX(int oldPosX)
        {

            this.oldPosX = oldPosX;

        }

        public int getOldPosY()
        {

            return this.oldPosY;

        }

        public void setOldPosY(int oldPosY)
        {

            this.oldPosY = oldPosY;

        }

        public void setPos()
        {

            Console.SetCursorPosition(this.getPosX(), this.getPosY());

        }

        public int getUpdateCounter()
        {

            return this.updateCounter;

        }

        public void setUpdateCounter(int updateCounter)
        {

            this.updateCounter = updateCounter;

        }

        public void incrementUpdateCounter(int amount)
        {

            this.setUpdateCounter(this.getUpdateCounter() + amount);

        }

        public void decrementUpdateCounter(int amount)
        {

            this.setUpdateCounter(this.getUpdateCounter() - amount);

        }

        public void incrementUpdateCounter()
        {

            this.incrementUpdateCounter(1);

        }

        public void decrementUpdateCounter()
        {

            this.decrementUpdateCounter(1);

        }

        public void resetUpdateCounter()
        {

            this.setUpdateCounter(0);

        }

    }

}