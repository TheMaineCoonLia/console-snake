﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleSnake
{

    class EntityManager
    {

        private readonly List<Entity> entities = new List<Entity>();

        public void spawn(Entity entity)
        {

            this.getEntities().Add(entity);

        }

        public void render()
        {

            for (int i = 0; i < this.getEntities().Count; i++)
            {

                this.getEntities()[i].render();

            }

        }

        public void update()
        {

            for(int i = 0; i < this.getEntities().Count; i++)
            {

                this.getEntities()[i].update();

            }

        }

        public Entity getEntityAt(int x, int y)
        {

            for (int i = 0; i < this.getEntities().Count; i++)
            {

                Entity entity = this.getEntities()[i];

                if (entity.getPosX() == x && entity.getPosY() == y)
                {

                    return entity;

                }

            }

            return null;

        }

        public List<Entity> getEntities()
        {

            return this.entities;

        }

    }

}