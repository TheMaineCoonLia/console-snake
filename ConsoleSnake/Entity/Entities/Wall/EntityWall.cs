﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleSnake
{
    
    class EntityWall : Entity
    {

        public EntityWall(int posX, int posY) : base("EntityWall")
        {

            this.setPosX(posX);
            this.setPosY(posY);

        }

        public override void render()
        {

            this.setPos();
            Console.BackgroundColor = ConsoleColor.DarkGray;
            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.Write("#");

        }

    }

}