﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleSnake
{

    class EntityPlayer : LivingEntity
    {

        private ConsoleColor color;

        public EntityPlayer(String name, ConsoleColor color)
            : base(name, 20F)
        {

            this.setColor(color);
            this.setPosX(Console.WindowWidth / 2);
            this.setPosY(Console.WindowHeight / 2);

        }

        public override void onColide(Entity entity)
        {
            
            throw new NotImplementedException();
        
        }

        public override void render()
        {

            this.setPos();
            Console.ForegroundColor = this.getColor();
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write("@");
            base.render();

        }

        public ConsoleColor getColor()
        {

            return this.color;

        }

        private void setColor(ConsoleColor color)
        {

            this.color = color;

        }

    }

}