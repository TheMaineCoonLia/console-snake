﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleSnake
{

    abstract class LivingEntity : Entity
    {

        private float health;

        public LivingEntity(String name, float health)
            : base(name)
        {

            this.setHealth(health);

        }

        abstract public void onColide(Entity entity);

        public float getHealth()
        {

            return this.health;

        }

        public void setHealth(float health)
        {

            this.health = health;

        }

        public Boolean isDead()
        {

            return this.getHealth() <= 0;

        }

        public void kill()
        {

            this.setHealth(0F);

        }

    }

}