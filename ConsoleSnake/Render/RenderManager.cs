﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleSnake
{
    class RenderManager
    {

        public void render()
        {

            if(ConsoleSnake.getInstance().getUI() != null)
            {

                ConsoleSnake.getInstance().getUI().render();

                for(int i = 0; i < ConsoleSnake.getInstance().getUI().getControlObjects().Count; i++)
                {

                    ConsoleSnake.getInstance().getUI().getControlObjects()[i].render();

                }

            }

            if(ConsoleSnake.getInstance().getGamemode() != null)
            {

                ConsoleSnake.getInstance().getGamemode().render();

            }

        }

    }
}