﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleSnake
{
    
    interface IInputController : IUpdateable
    {

        void handle(ConsoleKey key);

        ConsoleKey[] getKeys();

    }

}