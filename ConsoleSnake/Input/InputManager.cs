﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleSnake
{

    class InputManager
    {

        private readonly List<IInputController> controllers = new List<IInputController>();

        public void register(IInputController controller)
        {

            this.getControllers().Add(controller);

        }

        public void poll()
        {

            while(Console.KeyAvailable)
            {

                ConsoleKeyInfo key = Console.ReadKey(true);

                for(int i = 0; i < this.getControllers().Count; i++)
                {

                    IInputController controller = this.getControllers()[i];

                    for(int j = 0; j < controller.getKeys().Length; j++)
                    {

                        if(controller.getKeys()[j].Equals(key.Key))
                        {

                            controller.handle(key.Key);

                        }

                    }

                }

                if(ConsoleSnake.getInstance().getUI() != null)
                {

                    ConsoleSnake.getInstance().getUI().onKeyHit(key);

                }

            }

        }

        public List<IInputController> getControllers()
        {

            return this.controllers;

        }

    }

}