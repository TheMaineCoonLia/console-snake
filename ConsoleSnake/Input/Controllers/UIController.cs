﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleSnake
{
    class UIController : IInputController
    {

        private readonly ConsoleKey[] keys = new ConsoleKey[]
        {

            ConsoleKey.UpArrow,
            ConsoleKey.RightArrow,
            ConsoleKey.DownArrow,
            ConsoleKey.LeftArrow,
            ConsoleKey.Enter,
            ConsoleKey.Escape

        };
        private int updateCounter;

        public void handle(ConsoleKey key)
        {

            if(ConsoleSnake.getInstance().getUI() == null)
            {

                return;

            }

            ControlObject selectedObject = null;

            for(int i = 0; i < ConsoleSnake.getInstance().getUI().getControlObjects().Count; i++)
            {

                if (selectedObject != null)
                {

                    continue;

                }

                ControlObject controlObject = ConsoleSnake.getInstance().getUI().getControlObjects()[i];

                if (controlObject.isSelected())
                {

                    selectedObject = controlObject;

                }

            }

            if (selectedObject == null)
            {

                return;

            }

            ControlObject controlObj = null;

            switch (key)
            {

                case ConsoleKey.UpArrow:

                    controlObj = this.getObjectOver(selectedObject);

                    if (controlObj != null)
                    {

                        controlObj.setSelected(true);
                        selectedObject.setSelected(false);

                    }

                    break;

                case ConsoleKey.RightArrow:

                    controlObj = this.getObjectRight(selectedObject);

                    if (controlObj != null)
                    {

                        controlObj.setSelected(true);
                        selectedObject.setSelected(false);

                    }

                    break;

                case ConsoleKey.DownArrow:

                    controlObj = this.getObjectUnder(selectedObject);

                    if (controlObj != null)
                    {

                        controlObj.setSelected(true);
                        selectedObject.setSelected(false);

                    }

                    break;

                case ConsoleKey.LeftArrow:

                    controlObj = this.getObjectLeft(selectedObject);

                    if (controlObj != null)
                    {

                        controlObj.setSelected(true);
                        selectedObject.setSelected(false);

                    }

                    break;

                case ConsoleKey.Enter:

                    if(selectedObject is Button)
                    {

                        ConsoleSnake.getInstance().getUI().onAction(((Button) selectedObject).getCode());

                    }
                    else if(selectedObject is ColorSelector)
                    {

                        ((ColorSelector) selectedObject).next();

                    }

                    break;

                case ConsoleKey.Escape:

                    break;

                default:

                    break;

            }

        }

        private ControlObject getObjectOver(ControlObject over)
        {

            ControlObject lowest = new Button(-1, "LOWEST", -1, -1, over.getAlignment());

            for (int i = 0; i < ConsoleSnake.getInstance().getUI().getControlObjects().Count; i++)
            {

                ControlObject controlObject = ConsoleSnake.getInstance().getUI().getControlObjects()[i];

                if ((controlObject.getY() > lowest.getY()) && (controlObject.getY() < over.getY()))
                {

                    if (lowest.getAlignment() != controlObject.getAlignment())
                    {

                        continue;

                    }

                    lowest = controlObject;

                }

            }

            if (lowest.getX() != -1)
            {

                return lowest;

            }

            return null;

        }

        private ControlObject getObjectUnder(ControlObject under)
        {

            ControlObject highest = new Button(-1, "HIGHEST", -1, 10000, under.getAlignment());

            for (int i = 0; i < ConsoleSnake.getInstance().getUI().getControlObjects().Count; i++)
            {

                ControlObject controlObject = ConsoleSnake.getInstance().getUI().getControlObjects()[i];

                if ((controlObject.getY() < highest.getY()) && (controlObject.getY() > under.getY()))
                {

                    if (under.getAlignment() != controlObject.getAlignment())
                    {

                        continue;

                    }

                    highest = controlObject;

                }

            }

            if (highest.getX() != -1)
            {

                return highest;

            }

            return null;

        }

        private ControlObject getObjectRight(ControlObject right)
        {

            ControlObject closest = right;
            int row = right.getAlignment() + 1;

            for (int i = 0; i < ConsoleSnake.getInstance().getUI().getControlObjects().Count; i++)
            {

                ControlObject controlObject = ConsoleSnake.getInstance().getUI().getControlObjects()[i];

                if (controlObject.getAlignment() == row)
                {

                    if (controlObject.getY() <= right.getY())
                    {

                        closest = controlObject;

                    }

                }

            }

            if (closest.getX() != right.getX())
            {

                return closest;

            }

            return null;

        }

        private ControlObject getObjectLeft(ControlObject left)
        {

            ControlObject closest = left;
            int row = left.getAlignment() - 1;

            for (int i = 0; i < ConsoleSnake.getInstance().getUI().getControlObjects().Count; i++)
            {

                ControlObject controlObject = ConsoleSnake.getInstance().getUI().getControlObjects()[i];

                if (controlObject.getAlignment() == (row))
                {

                    if (controlObject.getY() <= left.getY())
                    {

                        closest = controlObject;

                    }

                }

            }

            if (closest.getX() != left.getX())
            {

                return closest;

            }

            return null;

        }

        public void update()
        {

        }

        public ConsoleKey[] getKeys()
        {

            return this.keys;

        }

        public int getUpdateCounter()
        {

            return this.updateCounter;

        }

        public void setUpdateCounter(int updateCounter)
        {

            this.updateCounter = updateCounter;

        }

        public void incrementUpdateCounter(int amount)
        {

            this.setUpdateCounter(this.getUpdateCounter() + amount);

        }

        public void decrementUpdateCounter(int amount)
        {

            this.setUpdateCounter(this.getUpdateCounter() - amount);

        }

        public void incrementUpdateCounter()
        {

            this.incrementUpdateCounter(1);

        }

        public void decrementUpdateCounter()
        {

            this.decrementUpdateCounter(1);

        }

        public void resetUpdateCounter()
        {

            this.setUpdateCounter(0);

        }

    }

}